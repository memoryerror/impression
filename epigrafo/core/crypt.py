from base64 import urlsafe_b64encode as b_encode
from hashlib import sha256

from cryptography.fernet import Fernet


class Crypter:
    def __init__(self, key):
        self.key = key

    def _encode_key(self, key):
        """Prepare key for use with cryptography.fernet.Fernet."""
        # Truncate hash to 32-bytes
        hashed_key = self.hash(key)[:32].encode()
        encoded_hash = b_encode(hashed_key)
        # Truncate encoded hash because Fernet requires key to be 32 bit.
        return encoded_hash

    def _encrypt(self, plaintext):
        """Encrypt Plaintext with AES 128-bit CBC Mode."""
        key = self._encode_key(self.key)
        encrypter = Fernet(key)
        ciphered = encrypter.encrypt(plaintext.encode())
        return ciphered.decode()

    def _decrypt(self, ciphered):
        """Decrypt AES 128-bit CBC Mode ciphered to plaintext."""
        key = self._encode_key(self.key)
        decrypter = Fernet(key)
        plaintext = decrypter.decrypt(ciphered)
        return plaintext.decode()

    def hash(self, plaintext):
        """Hash a plaintext string."""
        hashed = sha256(plaintext.encode()).hexdigest()
        return hashed

    def encrypt(self, *data):
        encrypted_data = [self._encrypt(item) for item in data]
        return encrypted_data

    def decrypt(self, *data):
        # Eval because strings are returned as strings that "look" like bytes, like 'b"hello world"' which seems like a bytes object.
        decrypted_data = [self._decrypt(item) for item in data]
        return decrypted_data
