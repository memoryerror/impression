from pathlib import Path

from kivy.lang.builder import Builder
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label
from kivy.uix.screenmanager import Screen
from kivy.uix.textinput import TextInput

from core import entry

style = str(Path("ui/styles/editor.kv"))
Builder.load_file(style)


class Editor(Screen):
    def save(self, title, content):
        manager = entry.EntryManager(self.password)
        manager.new(title, content)
