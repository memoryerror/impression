from pathlib import Path

from kivy.lang.builder import Builder
from kivy.uix.button import Button
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label
from kivy.uix.screenmanager import Screen

style = str(Path("ui/styles/menu.kv"))
Builder.load_file(style)


class Menu(Screen):
    def view_entries(self):
        view = self.manager.get_screen("View")
        view.setup(self.previews)
        self.manager.current = "View"

    def new_entry(self):
        editor = self.manager.get_screen("Editor")
        editor.password = self.password
        self.manager.current = "Editor"
