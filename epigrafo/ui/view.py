from pathlib import Path

from kivy.effects.scroll import ScrollEffect
from kivy.lang.builder import Builder
from kivy.properties import StringProperty
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.recycleview import RecycleView
from kivy.uix.screenmanager import Screen

style = str(Path("ui/styles/view.kv"))
Builder.load_file(style)


class EntryPreview(FloatLayout):
    title = StringProperty("")
    content = StringProperty("")


class View(Screen):
    def setup(self, data):
        self.main_view.data = data
